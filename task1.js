const express = require("express");
const app = express();
const rtAPIv1 = express.Router();
const bodyParser = require("body-parser");
const mongoose = require('mongoose');

var db = mongoose.createConnection('mongodb://localhost/taskMongoose');
var Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var userSchema = new Schema({
    name: String,
});

var taskSchema = new Schema({
    name: String,
    description: String,
    state: {
        type: String,
        enum: ["open", "close"]
    },
    user_id: ObjectId,
});

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

db.on("error", console.error.bind(console, "connection error:"));
db.once("open", function callback() {
    console.log("Connected!");

});

var User = db.model("User", userSchema);
var Task = db.model("Task", taskSchema);

/*
    Добавление задачи
    {"taskname": "taskname1", "taskdescr": "taskdescr1", "taskstate": "close", "taskuser": "123456789101"}
 */
rtAPIv1.post('/tasks', (req, res) => {
    taskObjects = {
        name: req.body.taskname,
        description: req.body.taskdescr,
        state: req.body.taskstate,
        user_id: req.body.taskuser
    };
    Task.create(taskObjects, function (err, result) {
        if (err) {
            console.log(err);
        } else {
            res.send(result);
        }
    });
}); //end of post, add new record
/*
    Просмотр всех задач
 */
rtAPIv1.get("/tasks", function (req, res) {
    Task.find({}, function (err, result) {
        if (err) {
            console.log(err);
        } else {
            res.send(result);
        }

    });
}); //end of get all records
/*
    Редактирование задачи
    {"taskname": "taskname1", "taskdescr": "taskdescr1", "taskstate": "close", "taskuser": "123456789101"}
 */
rtAPIv1.put('/tasks/:id', (req, res) => {
    taskObjects = {
        name: req.body.taskname,
        description: req.body.taskdescr,
        state: req.body.taskstate,
        user_id: req.body.taskuser
    };

    Task.update({_id: req.params.id}, {$set: taskObjects}, function (err, result) {
        if (err) {
            console.log(err);
        } else {
            if (result) {
                res.send(`Задача ${req.params.id} обновлена`);
            } else {
                res.status(400).send('Не найдены документы для обновления');
            }
        }
    });
});
/*
 Удаление задачи
 */
rtAPIv1.delete('/tasks/:id', (req, res) => {
    Task.remove({_id: req.params.id}, function (err, result) {
        if (err) {
            console.log(err);
        } else {
            res.send(`Задача - ${req.params.id} удалена`);
        }
    });
}); //end of delete record
/*
 Поиск задачи {"taskname": "taskname1", "taskdescr": "taskdescr1"}
 */
rtAPIv1.post('/tasks/search', (req, res) => {
    Task.find({$or: [{name: req.body.taskname}, {description: req.body.taskdescr}]}, function (err, result) {
            if (err) {
                console.log(err);
            } else {
                res.send(result);
            }
        });
}); //end of post, add new record
/*
 Смена статуса open/close
 {"taskstate": "open"}
 */
rtAPIv1.put('/tasks/state/:id', (req, res) => {
    taskObjects = {
        state: req.body.taskstate,
    };

    Task.update({_id: req.params.id}, {$set: taskObjects}, function (err, result) {
        if (err) {
            console.log(err);
        } else {
            if (result) {
                res.send(`Состояние задачи ${req.params.id} изменено на ${req.body.taskstate}`);
            } else {
                res.status(400).send('Не найдены документы для обновления');
            }
        }
    });
});
/*
 Делигирование задачи на пользователя
 {"taskuser": "4545454545"}
 */
rtAPIv1.put('/tasks/delegacy/:id', (req, res) => {
    taskObjects = {
        user_id: req.body.taskuser
    };

    Task.update({_id: req.params.id}, {$set: taskObjects}, function (err, result) {
        if (err) {
            console.log(err);
        } else {
            if (result) {
                res.send(`Задача ${req.params.id} делегирована пользователю ${req.body.taskuser}`);
            } else {
                res.status(400).send('Не найдены документы для обновления');
            }
        }
    });
});
/*
 Добавление пользователя
 {"username": "username1"}
 */
rtAPIv1.post('/users', (req, res) => {
    userObjects = {
        name: req.body.username,
    };
    User.create(userObjects, function (err, result) {
        if (err) {
            console.log(err);
        } else {
            res.send(result);
        }
    });
}); //end of post, add new record
/*
 Просмотр всех пользователей
 */
rtAPIv1.get("/users", function (req, res) {
    User.find({}, function (err, result) {
        if (err) {
            console.log(err);
        } else {
            res.send(result);
        }

    });
}); //end of get all records
/*
 Редактирование пользователя
 {"username": "username1"}
 */
rtAPIv1.put('/users/:id', (req, res) => {
    userObjects = {
        name: req.body.username,
    };

    User.update({_id: req.params.id}, {$set: userObjects}, function (err, result) {
        if (err) {
            console.log(err);
        } else {
            if (result) {
                res.send(`Имя пользователя ${req.params.id} изменено на ${req.body.username}`);
            } else {
                res.status(400).send('Не найдены документы для обновления');
            }
        }
    });

});
/*
 Удаление пользователя
 */
rtAPIv1.delete('/users/:id', (req, res) => {
    User.remove({_id: req.params.id}, function (err, result) {
        if (err) {
            console.log(err);
        } else {
            res.send(`Запись - ${req.params.id} удалена`);
        }
    });
}); //end of delete record

app.listen(3000);
app.use("/api/v1", rtAPIv1);

